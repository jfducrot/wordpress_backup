#!/bin/bash
source /usr/local/bin/backup/config.sh

tar cvzf $backup_directory/temp/archive.tgz /var/www/*

mysqldump -u $db_user -p$db_pwd $db_name > $backup_directory/temp/wordpress_db.sql

#calcul nom backup journalier
backup_date=`date +%Y%m%d`
constant_name='_backup.tgz'
backup_file=$backup_date$constant_name
echo $backup_file

#backup dans backup journalier

tar cvzf $backup_directory/$backup_file $backup_directory/temp/*

rm $backup_directory/temp/*